package com.example.springScafold.scafolding.security.representation;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
@Builder
public class UserDetailsRepresentation implements org.springframework.security.core.userdetails.UserDetails {

    private Long id;
    private String username;
    private String lastName;
    private String firstName;
    private String password;
    private Collection<GrantedAuthority> authorities;


    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + "HIDDEN" + '\'' +
                ", authorities=" + authorities +
                '}';
    }
}
