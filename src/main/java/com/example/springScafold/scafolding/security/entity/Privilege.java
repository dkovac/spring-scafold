package com.example.springScafold.scafolding.security.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "privileges")
@Getter
@Setter
@EqualsAndHashCode(of = "id", callSuper = false)
@ToString
public class Privilege {

    @Id
    private Long id;

    @Column(nullable = false)
    private String code;
    private String description;
}
