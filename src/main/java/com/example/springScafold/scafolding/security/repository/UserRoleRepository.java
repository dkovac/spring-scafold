package com.example.springScafold.scafolding.security.repository;

import com.example.springScafold.scafolding.security.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole, UserRole.UserRoleId> {

    @Query("FROM user_roles ur WHERE ur.user.id = :userId")
    List<UserRole> findAllByUserId(@Param("userId") Long userId);
}
