package com.example.springScafold.scafolding.security.controller;

import com.example.springScafold.scafolding.security.entity.Role;
import com.example.springScafold.scafolding.security.repository.RoleRepository;
import com.example.springScafold.scafolding.security.representation.AuthenticatedUserDataResponse;
import com.example.springScafold.scafolding.security.representation.AuthenticationRequest;
import com.example.springScafold.scafolding.security.representation.AuthenticationResponse;
import com.example.springScafold.scafolding.security.service.AuthenticationService;
import com.example.springScafold.scafolding.common.utils.AuthenticationUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/authenticate")
@Slf4j
@AllArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;
    private RoleRepository roleRepository;

    @PostMapping(
            value = "/login",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public AuthenticationResponse login(@RequestBody @Valid AuthenticationRequest request) {
        log.debug("Received login request for user: {}", request.getUsername());

        return authenticationService.login(request);
    }

    @PostMapping(
            value = "/logout",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("isAuthenticated()")
    public AuthenticationResponse logout() {
        log.debug("Received logout request for user: {}", AuthenticationUtils.getAuthenticatedUser().getUsername());

        return authenticationService.logout();
    }

    @GetMapping(
            value = "/me",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("isAuthenticated()")
    public AuthenticatedUserDataResponse getAuthenticatedUser() {
        log.debug("Received getAuthenticatedUser request for user: {}",
                AuthenticationUtils.getAuthenticatedUser().getUsername());

        return authenticationService.getAuthenticatedUser();
    }

    @GetMapping(value="/roles")
   // @PreAuthorize("isAuthenticated()")
    public List<Role> getUserRoles() {

        return roleRepository.findAll();
    }
}
