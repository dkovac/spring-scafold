package com.example.springScafold.scafolding.security.repository;

import com.example.springScafold.scafolding.security.entity.RolePrivilege;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolePrivilegeRepository extends JpaRepository<RolePrivilege, RolePrivilege.RolePrivilegeId> {
}
