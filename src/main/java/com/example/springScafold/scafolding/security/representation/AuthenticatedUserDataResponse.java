package com.example.springScafold.scafolding.security.representation;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id", callSuper = false)
@Builder
public class AuthenticatedUserDataResponse {

    private Long id;
    private String username;
    private String givenName;
    private String familyName;
    private String email;
    private List<String> authorities;


}
