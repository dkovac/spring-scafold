package com.example.springScafold.scafolding.security.representation;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AuthenticationResponse {

    private String jwtToken;
    private AuthenticatedUserDataResponse user;
}
