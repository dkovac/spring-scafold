package com.example.springScafold.scafolding.security.repository;

import com.example.springScafold.scafolding.security.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("FROM Role r WHERE r.id IN :ids")
    List<Role> fetchRolesFromIds(@Param("ids") List<Long> ids);

    @Query(value = "SELECT * " +
            "FROM Roles r " +
            "WHERE r.code = :roleCode",
            nativeQuery = true)
    Role findByCode(@Param("roleCode") String roleCode);
}
