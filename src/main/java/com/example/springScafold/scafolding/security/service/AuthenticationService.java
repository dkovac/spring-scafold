package com.example.springScafold.scafolding.security.service;


import com.example.springScafold.scafolding.security.representation.AuthenticatedUserDataResponse;
import com.example.springScafold.scafolding.security.representation.AuthenticationRequest;
import com.example.springScafold.scafolding.security.representation.AuthenticationResponse;

public interface AuthenticationService {

    AuthenticationResponse login(AuthenticationRequest request);

    AuthenticationResponse logout();

    AuthenticatedUserDataResponse getAuthenticatedUser();
}
