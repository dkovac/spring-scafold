package com.example.springScafold.scafolding.security.entity;

import com.example.springScafold.scafolding.user.entity.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Table
@Entity(name = "user_roles")
public class UserRole {

    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.TABLE)
    private UserRoleId id;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private Role role;

    public UserRole() {
        id = new UserRoleId();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.id.userId = user.getId();
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.id.roleId = role.getId();
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + id +
                ", user=" + user +
                ", role=" + role +
                '}';
    }

    @Embeddable
    public static class UserRoleId implements Serializable {

        @Column(name = "user_id")
        Long userId;
        @Column(name = "role_id")
        Long roleId;

        public UserRoleId() {

        }

        public UserRoleId(Long userId, Long roleId) {
            this.userId = userId;
            this.roleId = roleId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            UserRoleId that = (UserRoleId) o;

            if (!Objects.equals(userId, that.userId)) return false;
            return Objects.equals(roleId, that.roleId);
        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "UserRoleId{" +
                    "roleId=" + userId +
                    ", privilegeId=" + roleId +
                    '}';
        }
    }
}
