package com.example.springScafold.scafolding.security.config;

import com.example.springScafold.scafolding.common.utils.JwtUtils;
import com.example.springScafold.scafolding.security.ex.InvalidTokenException;
import com.example.springScafold.scafolding.security.representation.UserDetailsRepresentation;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
@Slf4j
public class TokenProvider {

    private static final String AUTHORITIES_KEY = "auth";
    private static final String ID = "id";

    // vidit s Reljom ove pizdarije !!!!!
    @Value("${project.security.jwt.secretKey}")
    private String secretKey;

    @Value("#{new Integer('${project.security.jwt.tokenValidityInSeconds}') * 1000 }")
    private long tokenValidityInMilliseconds;

    @Value("#{new Integer('${project.security.jwt.tokenValidityInSecondsForRememberMe}') * 1000 }")
    private long tokenValidityInMillisecondsForRememberMe;

    public String createToken(Authentication authentication, boolean rememberMe) {
        String authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        long now = (new Date()).getTime();
        Date validity;
        if (rememberMe) {
            validity = new Date(now + this.tokenValidityInMillisecondsForRememberMe);
        } else {
            validity = new Date(now + this.tokenValidityInMilliseconds);
        }

        UserDetailsRepresentation principal = (UserDetailsRepresentation) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(ID, principal.getId())
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(validity)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();

        // extract authorities from the JWT claims
        String auth = claims.get(AUTHORITIES_KEY).toString();

        Collection<GrantedAuthority> authorities = Arrays
                .stream(auth.split(",")).filter(StringUtils::isNotBlank).map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        UserDetailsRepresentation principal = UserDetailsRepresentation.builder()
                .id(claims.get(ID, Long.class))
                .username(claims.getSubject())
                .password("")
                .authorities(authorities)
                .build();

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    public void validateToken(String authToken) throws InvalidTokenException {
        JwtUtils.validateToken(authToken, secretKey);
    }
}
