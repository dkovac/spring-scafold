package com.example.springScafold.scafolding.security.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Table
@Entity(name = "role_privileges")
public class RolePrivilege {

    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.TABLE)
    private RolePrivilegeId id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(name = "privilege_id", insertable = false, updatable = false)
    private Privilege privilege;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.id.roleId = role.getId();
        this.role = role;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void setRole(Privilege privilege) {
        this.id.privilegeId = privilege.getId();
        this.privilege = privilege;
    }

    @Override
    public String toString() {
        return "RolePrivilege{" +
                "id=" + id +
                ", role=" + role +
                ", privilege=" + privilege +
                '}';
    }

    @Embeddable
    public static class RolePrivilegeId implements Serializable {

        @Column(name = "role_id")
        Long roleId;
        @Column(name = "privilege_id")
        Long privilegeId;

        public RolePrivilegeId() {

        }

        public RolePrivilegeId(Long roleId, Long privilegeId) {
            this.roleId = roleId;
            this.privilegeId = privilegeId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RolePrivilegeId that = (RolePrivilegeId) o;

            if (!Objects.equals(roleId, that.roleId)) return false;
            return Objects.equals(privilegeId, that.privilegeId);
        }

        @Override
        public int hashCode() {
            int result = roleId != null ? roleId.hashCode() : 0;
            result = 31 * result + (privilegeId != null ? privilegeId.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "RolePrivilegeId{" +
                    "roleId=" + roleId +
                    ", privilegeId=" + privilegeId +
                    '}';
        }
    }
}
