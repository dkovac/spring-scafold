package com.example.springScafold.scafolding.security.service;

import com.example.springScafold.scafolding.common.utils.AuthenticationUtils;
import com.example.springScafold.scafolding.security.config.TokenProvider;
import com.example.springScafold.scafolding.security.entity.Privilege;
import com.example.springScafold.scafolding.security.representation.AuthenticatedUserDataResponse;
import com.example.springScafold.scafolding.security.representation.AuthenticationRequest;
import com.example.springScafold.scafolding.security.representation.AuthenticationResponse;
import com.example.springScafold.scafolding.security.representation.UserDetailsRepresentation;
import com.example.springScafold.scafolding.user.entity.User;
import com.example.springScafold.scafolding.user.service.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final TokenProvider jwtTokenProvider;
    private final UserRepository userRepository;

    @Override
    public AuthenticationResponse login(AuthenticationRequest request) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            final String token = jwtTokenProvider.createToken(authentication, false);

            return AuthenticationResponse.builder()
                    .jwtToken(token)
                    .user(authenticatedUser(request.getUsername()))
                    .build();
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }

    @Override
    public AuthenticationResponse logout() {
        SecurityContextHolder.clearContext();
        return AuthenticationResponse.builder()
                .build();
    }

    @Override
    public AuthenticatedUserDataResponse getAuthenticatedUser() {
        final UserDetailsRepresentation userDetails = AuthenticationUtils.getAuthenticatedUser();
        log.debug("Received get authenticated user request for: {}", userDetails.getUsername());

        return authenticatedUser(userDetails.getUsername());
    }

    private AuthenticatedUserDataResponse authenticatedUser(String username) {
        final User user = userRepository
                .findByUsernameOrEmail(username)
                .orElseThrow(() -> new InsufficientAuthenticationException("Authenticated user not found"));

        return AuthenticationUtils.createAuthenticatedUserRepresentation(user);
    }
}
