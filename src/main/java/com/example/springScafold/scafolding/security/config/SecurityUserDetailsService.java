package com.example.springScafold.scafolding.security.config;

import com.example.springScafold.scafolding.common.utils.AuthenticationUtils;
import com.example.springScafold.scafolding.security.representation.UserDetailsRepresentation;
import com.example.springScafold.scafolding.user.entity.User;
import com.example.springScafold.scafolding.user.service.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component("userDetailsService")
@AllArgsConstructor
public class SecurityUserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository
                .findByUsernameOrEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        return UserDetailsRepresentation.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(
                        AuthenticationUtils.extractAuthoritiesFromRoles(user.getRoles()).stream()
                                .map(SimpleGrantedAuthority::new)
                                .collect(Collectors.toList())
                )
                .build();
    }
}