package com.example.springScafold.scafolding.common.service;

import com.example.springScafold.scafolding.common.entity.AuditableEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

@NoRepositoryBean
public interface ProjectRepository<T extends AuditableEntity> extends JpaRepository<T, Long> {
    @Query("FROM #{#entityName} t WHERE t.deleted = false")
    Page<T> fetchAll(Pageable pageable);

    @Query("FROM #{#entityName} t WHERE t.id = :id AND t.deleted = false")
    Optional<T> fetchById(@Param("id") Long id);
}
