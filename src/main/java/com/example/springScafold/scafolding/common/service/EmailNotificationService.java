package com.example.springScafold.scafolding.common.service;

import com.example.springScafold.scafolding.common.representation.Recipient;
import com.example.springScafold.scafolding.common.utils.FileUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailNotificationService implements NotificationService {
    private static final String NAME_PLACEHOLDER = "[name]";
    private static final String MESSAGE_PLACEHOLDER = "[message]";
    private static final String DETAILS_PLACEHOLDER = "[details]";
    private static final String SIGNATURE_PLACEHOLDER = "[signature]";
    private static final String NOTIFICATIONS_SIGNATURE_KEY = "notifications.signature";
    private static final String UTF_8 = "utf-8";
    private static final String MAIL_MIME_CHARSET_KEY = "mail.mime.charset";
    private static final String LINK_PATTERN = "<a href=\"%s\">Link</a>";

    @Value("${project.notifications.enabled:true}")
    private Boolean notificationsEnabled;

    @Value("${project.notifications.sender}")
    private String sender;

    @Value("${project.notifications.basePath}")
    private String basePath;

    @Value("${project.notifications.resetPasswordPath}")
    private String resetPasswordPath;

    @Value("${spring.mvc.locale}")
    private Locale locale;

    private final JavaMailSender javaMailSender;
    private final MessageSource messageSource;

    @PostConstruct
    public void init() {
        System.setProperty(MAIL_MIME_CHARSET_KEY, UTF_8);
    }

    @Override
    public void notifyAboutUserCreation(List<Recipient> recipients, String username, String password) {
        final String message = messageSource.getMessage(
                "notifications.messages.user.create.message",
                new Object[0],
                locale);

        final String details = messageSource.getMessage(
                "notifications.messages.user.create.details",
                new Object[]{
                        username,
                        password,
                        String.format(LINK_PATTERN, basePath)
                },
                locale);

        sendEmailNotification(recipients, message, details);
    }

    @Override
    public void notifyAboutPasswordChange(List<Recipient> recipients, ZonedDateTime timestamp) {
        final String formattedTimestamp = timestamp.format(
                DateTimeFormatter
                        .ofLocalizedDateTime(FormatStyle.MEDIUM)
                        .withLocale(locale));

        final String message = messageSource.getMessage(
                "notifications.messages.user.passwordChange.message",
                new Object[0],
                locale);

        final String details = messageSource.getMessage(
                "notifications.messages.user.passwordChange.details",
                new Object[]{formattedTimestamp},
                locale);

        sendEmailNotification(recipients, message, details);
    }


    private void sendEmailNotification(List<Recipient> recipients, String message, String details) {
        if (notificationsEnabled != null && notificationsEnabled) {
            log.info("Sending notifications to {} recipients", recipients.size());
            log.debug("Recipients: {}", recipients);

            recipients.forEach(recipient -> {
                try {
                    final MimeMessage msg = javaMailSender.createMimeMessage();

                    String emailText = fillTemplate(
                            messageSource.getMessage("notifications.messages.intro",
                                    new Object[]{recipient.getName()},
                                    locale),
                            message,
                            details,
                            messageSource.getMessage(NOTIFICATIONS_SIGNATURE_KEY,
                                    new Object[0],
                                    locale));

                    if (emailText == null) {
                        emailText = "<p>Error occurred</p>";
                    }

                    msg.setFrom(sender);
                    msg.setRecipients(Message.RecipientType.TO, recipient.getAddress());
                    msg.setSubject("[BrainIT] " + message, UTF_8);
                    msg.setText(emailText, UTF_8, "html");

                    javaMailSender.send(msg);
                } catch (MessagingException | MailSendException e) {
                    log.error("Error occurred while sending email: " + e);
                }
            });
        } else {
            log.debug("Skipping sending notifications");
        }
    }

        private String fillTemplate(String name, String message, String details, String signature) {
            final String template = FileUtils.getFile("/email/template.html");
            if (template != null) {
                return template
                        .replace(NAME_PLACEHOLDER, name)
                        .replace(MESSAGE_PLACEHOLDER, message)
                        .replace(DETAILS_PLACEHOLDER, details)
                        .replace(SIGNATURE_PLACEHOLDER, signature);
            } else {
                return null;
            }
        }

}
