package com.example.springScafold.scafolding.common.controller;

import com.example.springScafold.scafolding.common.ex.ApplicationEntityNotFoundException;
import com.example.springScafold.scafolding.common.representation.ErrorDetails;
import com.example.springScafold.scafolding.common.representation.ErrorField;
import com.example.springScafold.scafolding.common.representation.ValidationErrorDetails;
import com.example.springScafold.scafolding.security.ex.MissingDataException;
import com.example.springScafold.scafolding.user.ex.EmptyRolesException;
import com.example.springScafold.scafolding.user.ex.PasswordTokenExpiredException;
import com.example.springScafold.scafolding.user.ex.PasswordTokenInvalidException;
import com.example.springScafold.scafolding.user.ex.UserException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Locale;
import java.util.stream.Collectors;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ProjectExceptionHandler {
    private static final String ROLE_FIELD = "roles";

    private final MessageSource messageSource;

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ErrorDetails handleAllExceptions(Exception ex, WebRequest request) {
        log.error("Exception occurred:", ex);
        return ErrorDetails.builder()
                .timestamp(ZonedDateTime.now())
                .message(request.getDescription(false))
                .details(ex.getMessage())
                .build();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(MissingDataException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ErrorDetails handleMissingData(MissingDataException ex) {
        log.error("Exception occurred:", ex);
        return errorResponse(ex, "exception.missingData.message");
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ValidationErrorDetails handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {

        return ValidationErrorDetails.builder()
                .timestamp(ZonedDateTime.now())
                .message(i18n("exception.validationFailed.message"))
                .errors(
                        ex.getBindingResult().getAllErrors().stream()
                                .map(error -> ErrorField.builder()
                                        .fieldName(((FieldError) error).getField())
                                        .error(error.getDefaultMessage())
                                        .build())
                                .collect(Collectors.toList()))
                .build();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(EmptyRolesException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ValidationErrorDetails handleEmptyRolesException() {
        return ValidationErrorDetails.builder()
                .timestamp(ZonedDateTime.now())
                .message(i18n("exception.validationFailed.message"))
                .errors(Collections.singletonList(
                        ErrorField.builder()
                                .fieldName(ROLE_FIELD)
                                .error(i18n("exception.emptyRoles.message"))
                                .build()
                ))
                .build();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(UserException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handleUserException(UserException ex) {
        return errorResponse(ex, ex.getValidationMessage());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorDetails handleAccessDenied(AccessDeniedException ex) {
        return errorResponse(ex, "exception.accessDenied.message");
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDetails handleBadCredentials(BadCredentialsException ex) {
        return errorResponse(ex, "exception.unauthorized.message");
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(ApplicationEntityNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handleApplicationEntityNotFound(ApplicationEntityNotFoundException ex) {
        return errorResponse(ex, "exception.generic.notFound.message");
    }

//    @org.springframework.web.bind.annotation.ExceptionHandler(EmailTakenException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    public ErrorDetails handleEmailTaken(EmailTakenException ex) {
//        return errorResponse(ex, "exception.emailTaken.message");
//    }
//
//    @org.springframework.web.bind.annotation.ExceptionHandler(UsernameTakenException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    public ErrorDetails handleUsernameTaken(UsernameTakenException ex) {
//        return errorResponse(ex, "exception.usernameTaken.message");
//    }


    @org.springframework.web.bind.annotation.ExceptionHandler({
            PasswordTokenExpiredException.class,
            PasswordTokenInvalidException.class
    })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handlePasswordTokenExceptions(RuntimeException ex) {
        String key = "exception.passwordTokenInvalid.message";

        if (ex instanceof PasswordTokenExpiredException) {
            key = "exception.passwordTokenExpired.message";
        }

        return errorResponse(ex, key);
    }

    private ErrorDetails errorResponse(RuntimeException ex, String messageKey) {
        return ErrorDetails.builder()
                .timestamp(ZonedDateTime.now())
                .message(ex.getClass().getSimpleName())
                .details(i18n(messageKey))
                .build();
    }

    private String i18n(String key) {
        return messageSource.getMessage(key, new Object[0], Locale.getDefault());
    }
}
