package com.example.springScafold.scafolding.common.representation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@Builder
@ToString
public class Recipient {
    private String name;
    private String address;
}
