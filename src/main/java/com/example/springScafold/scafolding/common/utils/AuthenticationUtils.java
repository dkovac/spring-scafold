package com.example.springScafold.scafolding.common.utils;

import com.example.springScafold.scafolding.security.entity.Privilege;
import com.example.springScafold.scafolding.security.entity.Role;
import com.example.springScafold.scafolding.security.ex.MissingDataException;
import com.example.springScafold.scafolding.security.representation.AuthenticatedUserDataResponse;
import com.example.springScafold.scafolding.security.representation.UserDetailsRepresentation;
import com.example.springScafold.scafolding.user.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

public class AuthenticationUtils {

    private AuthenticationUtils() {
    }

    public static UserDetailsRepresentation getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object details = authentication.getPrincipal();

        if (details instanceof UserDetailsRepresentation) {
            return (UserDetailsRepresentation) details;
        } else {
            throw new MissingDataException("UserDetailsRepresentation missing in authentication security context.");
        }
    }

    public static boolean authenticatedUserHasAuthority(String authority) {
        final UserDetailsRepresentation authenticatedUser = getAuthenticatedUser();

        return authenticatedUser.getAuthorities().stream()
                .anyMatch(auth -> auth.getAuthority().equals(authority));
    }

    public static boolean authenticatedUserHasAuthorities(String... authorities) {
        for (String authority : authorities) {
            if (!authenticatedUserHasAuthority(authority)) {
                return false;
            }
        }
        return true;
    }

    public static AuthenticatedUserDataResponse createAuthenticatedUserRepresentation(User user) {
        return AuthenticatedUserDataResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .familyName(user.getFamilyName())
                .givenName(user.getGivenName())
                .email(user.getEmail())
                .authorities(extractAuthoritiesFromRoles(user.getRoles()))
                .build();
    }

    public static List<String> extractAuthoritiesFromRoles(List<Role> roles) {
        return roles.stream()
                .flatMap(role -> role.getPrivileges().stream())
                .filter(StreamUtils.distinctByKey(Privilege::getId))
                .map(Privilege::getCode)
                .collect(Collectors.toList());
    }
}
