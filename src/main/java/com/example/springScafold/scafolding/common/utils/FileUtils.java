package com.example.springScafold.scafolding.common.utils;

import com.example.springScafold.scafolding.common.ex.MalformedFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class FileUtils {
    private static final Logger log = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
    }

    public static String getFile(String path) {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(FileUtils.class.getResourceAsStream(path)))) {
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException ex) {
            log.error("Error occurred while getting file from path {}:  {}", path, ex);
        }
        return null;
    }

    public static void checkIfEmpty(MultipartFile file) {
        if (file.isEmpty()) {
            log.error("Malformed file with name: {}", file.getName());
            throw new MalformedFileException();
        }
    }
}
