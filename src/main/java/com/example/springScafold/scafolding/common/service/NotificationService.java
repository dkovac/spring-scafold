package com.example.springScafold.scafolding.common.service;

import com.example.springScafold.scafolding.common.representation.Recipient;

import java.time.ZonedDateTime;
import java.util.List;

public interface NotificationService {
    void notifyAboutUserCreation(List<Recipient> recipients, String username, String password);
    void notifyAboutPasswordChange(List<Recipient> recipients, ZonedDateTime timestamp);
}
