package com.example.springScafold.scafolding.common.representation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class ValidationErrorDetails {
    private ZonedDateTime timestamp;
    private String message;
    private List<ErrorField> errors;
}
