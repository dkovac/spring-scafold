package com.example.springScafold.scafolding.common.utils;

import com.example.springScafold.scafolding.security.ex.InvalidTokenException;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class JwtUtils {

    private JwtUtils() {
    }

    @SuppressWarnings("PlaceholderCountMatchesArgumentCount")
    public static void validateToken(String token, String secretKey) throws InvalidTokenException {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        } catch (SignatureException e) {
            log.info("Invalid JWT signature.");
            log.trace("Invalid JWT signature trace: {}", e);
            throw new InvalidTokenException("The access token signature is invalid", e);
        } catch (MalformedJwtException e) {
            log.info("Invalid JWT token.");
            log.trace("Invalid JWT token trace: {}", e);
            throw new InvalidTokenException("The token is malformed", e);
        } catch (ExpiredJwtException e) {
            throw new InvalidTokenException("The token expired", e);
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT token.");
            log.trace("Unsupported JWT token trace: {}", e);
            throw new InvalidTokenException("The access token is unsupported", e);
        }
    }

}
