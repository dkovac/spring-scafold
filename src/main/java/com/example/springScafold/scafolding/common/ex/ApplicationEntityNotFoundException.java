package com.example.springScafold.scafolding.common.ex;

public class ApplicationEntityNotFoundException extends RuntimeException {
    public ApplicationEntityNotFoundException(String entityName, Long id) {
        super(entityName + " with id " + id + " not found");
    }

    public ApplicationEntityNotFoundException(String message) {
        super(message);
    }
}
