package com.example.springScafold.scafolding.common.representation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@AllArgsConstructor
@Value
@Builder
@ToString
public class ErrorField {
    private String fieldName;
    private String error;
}
