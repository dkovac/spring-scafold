package com.example.springScafold.scafolding.user.model;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class RoleRequest {

    @NotNull(message = "{validation.generic.notNull}")
    private Long roleId;

}
