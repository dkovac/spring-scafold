package com.example.springScafold.scafolding.user.ex;


import com.example.springScafold.scafolding.common.ex.ApplicationEntityNotFoundException;

public class RoleNotFoundException extends ApplicationEntityNotFoundException {
    private static final String ENTITY_NAME = "Role";

    public RoleNotFoundException(Long id) {
        super(ENTITY_NAME, id);
    }
}
