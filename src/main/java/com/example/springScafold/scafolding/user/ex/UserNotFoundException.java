package com.example.springScafold.scafolding.user.ex;

import com.example.springScafold.scafolding.common.ex.ApplicationEntityNotFoundException;

public class UserNotFoundException extends ApplicationEntityNotFoundException {
    private static final String ENTITY_NAME = "User";

    public UserNotFoundException(Long id) {
        super(ENTITY_NAME, id);
    }

    public UserNotFoundException(String email) {
        super("User with email " + email + " not found");
    }
}
