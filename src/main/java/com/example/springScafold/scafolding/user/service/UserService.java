package com.example.springScafold.scafolding.user.service;


import com.example.springScafold.scafolding.user.model.ChangePasswordRequest;
import com.example.springScafold.scafolding.user.model.ChangeUserPasswordRequest;
import com.example.springScafold.scafolding.user.model.RoleRepresentation;

import java.util.List;

public interface UserService {
    void changePassword(ChangePasswordRequest request);

    void changeUserPassword(ChangeUserPasswordRequest request);

    void addRoleToUser(Long userId, Long roleId);

    void removeRoleFromUser(Long userId, Long roleId);

    List<RoleRepresentation> getRoles();
}
