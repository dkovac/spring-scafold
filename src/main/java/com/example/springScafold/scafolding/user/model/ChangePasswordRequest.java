package com.example.springScafold.scafolding.user.model;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
public class ChangePasswordRequest {

    @Pattern(regexp = "^[_.@A-Za-z0-9-]*$", message = "{validation.person.password.pattern}")
    @NotNull(message = "{validation.generic.notNull}")
    @Size(min = 6, max = 120, message = "{validation.person.password.size}")
    private String currentPassword;

    @Pattern(regexp = "^[_.@A-Za-z0-9-]*$", message = "{validation.person.password.pattern}")
    @NotNull(message = "{validation.generic.notNull}")
    @Size(min = 6, max = 120, message = "{validation.person.password.size}")
    private String newPassword;
}
