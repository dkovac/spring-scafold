package com.example.springScafold.scafolding.user.ex;

public class PasswordTokenInvalidException extends RuntimeException {

    public PasswordTokenInvalidException(String message) {
        super(message);
    }

}
