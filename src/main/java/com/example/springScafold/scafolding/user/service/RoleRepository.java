package com.example.springScafold.scafolding.user.service;

import com.example.springScafold.scafolding.security.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("FROM Role r WHERE r.id IN :ids")
    List<Role> fetchRolesFromIds(@Param("ids") List<Long> ids);
}
