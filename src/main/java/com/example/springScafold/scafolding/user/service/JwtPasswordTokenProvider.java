package com.example.springScafold.scafolding.user.service;

import com.example.springScafold.scafolding.common.utils.JwtUtils;
import com.example.springScafold.scafolding.security.ex.InvalidTokenException;
import com.example.springScafold.scafolding.user.ex.PasswordTokenExpiredException;
import com.example.springScafold.scafolding.user.ex.PasswordTokenInvalidException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class JwtPasswordTokenProvider implements PasswordTokenProvider {

    private static final String ID = "id";
    private static final String RESET_PASSWORD_SUBJECT = "ResetPassword";

    @Value("${project.security.jwt.secretKey}")
    private String secretKey;

    @Value("${project.passwordReset.tokenValidity}")
    private long passwordResetTokenValidityInSeconds;

    @Override
    public String createToken(Long id) {
        long now = (new Date()).getTime();
        Date validity = new Date(now + (passwordResetTokenValidityInSeconds * 1000));

        return Jwts.builder()
                .setSubject(RESET_PASSWORD_SUBJECT)
                .claim(ID, id)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(validity)
                .compact();
    }

    @Override
    public void validateToken(String token) {
        try {
            JwtUtils.validateToken(token, secretKey);
        } catch (InvalidTokenException e) {
            log.error("Error occurred: {}", e.getMessage());
            if (e.getCause() instanceof ExpiredJwtException) {
                throw new PasswordTokenExpiredException(e.getMessage());
            } else {
                throw new PasswordTokenInvalidException(e.getMessage());
            }
        }
    }

    @Override
    public Long getUserId(String token) {
        final Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        return claims.get(ID, Long.class);
    }

    @Override
    public Long getTokenValidity() {
        return passwordResetTokenValidityInSeconds;
    }

}
