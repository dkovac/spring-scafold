package com.example.springScafold.scafolding.user.service;

import com.example.springScafold.scafolding.common.service.NotificationService;
import com.example.springScafold.scafolding.security.entity.Role;
import com.example.springScafold.scafolding.security.entity.UserRole;
import com.example.springScafold.scafolding.user.entity.User;
import com.example.springScafold.scafolding.user.ex.RoleNotFoundException;
import com.example.springScafold.scafolding.user.ex.UserException;
import com.example.springScafold.scafolding.user.ex.UserNotFoundException;
import com.example.springScafold.scafolding.user.model.ChangePasswordRequest;
import com.example.springScafold.scafolding.security.repository.UserRoleRepository;
import com.example.springScafold.scafolding.security.representation.UserDetailsRepresentation;
import com.example.springScafold.scafolding.user.model.ChangeUserPasswordRequest;
import com.example.springScafold.scafolding.user.model.RoleRepresentation;
import com.example.springScafold.scafolding.common.utils.AuthenticationUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.stream.Collectors;
import java.util.List;

import static com.example.springScafold.scafolding.user.utils.UserUtils.recipients;


@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private static final String INVALID_PASSWORD_VALIDATION_KEY = "validation.user.password.match";

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordTokenProvider passwordTokenProvider;
    private final NotificationService notificationService;

    @Override
    public void changePassword(ChangePasswordRequest request) {
        final UserDetailsRepresentation authenticatedUser = AuthenticationUtils.getAuthenticatedUser();
        final User user = getUser(authenticatedUser.getId());

        if (!passwordEncoder.matches(request.getCurrentPassword(), user.getPassword())) {
            throw new UserException(
                    "Given password is not the same as the current password", INVALID_PASSWORD_VALIDATION_KEY);
        }

        user.setPassword(passwordEncoder.encode(request.getNewPassword()));
        userRepository.save(user);
        notificationService.notifyAboutPasswordChange(recipients(user), ZonedDateTime.now());
    }

    @Override
    public void changeUserPassword(ChangeUserPasswordRequest request) {
        final User user = getUser(request.getId());
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));
        userRepository.save(user);
        notificationService.notifyAboutPasswordChange(recipients(user), ZonedDateTime.now());
    }

    @Override
    public void addRoleToUser(Long userId, Long roleId) {
        final User user = userRepository
                .fetchById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));

        final Role role = roleRepository
                .findById(roleId)
                .orElseThrow(() -> new RoleNotFoundException(userId));

        final UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(role);
        userRoleRepository.save(userRole);
    }

    @Override
    public void removeRoleFromUser(Long userId, Long roleId) {
        UserRole.UserRoleId userRoleId = new UserRole.UserRoleId(userId, roleId);
        userRoleRepository.deleteById(userRoleId);
    }

    @Override
    public List<RoleRepresentation> getRoles() {
        return roleRepository.findAll().stream()
                .map(r -> RoleRepresentation.builder()
                        .id(r.getId())
                        .code(r.getCode())
                        .description(r.getDescription())
                        .build())
                .collect(Collectors.toList());
    }

    private User getUser(Long id) {
        return userRepository
                .fetchById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

}
