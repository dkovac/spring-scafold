package com.example.springScafold.scafolding.user.utils;


import com.example.springScafold.scafolding.common.representation.Recipient;
import com.example.springScafold.scafolding.user.entity.User;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserUtils {

    private UserUtils() {
    }

    public static List<Recipient> recipients(User... users) {
        return Stream.of(users)
                .map(user -> Recipient.builder()
                        .name(user.getGivenName() + " " + user.getFamilyName())
                        .address(user.getEmail())
                        .build())
                .collect(Collectors.toList());
    }

    public static String generateRandomPassword() {
        final String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
        final String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
        final String numbers = RandomStringUtils.randomNumeric(2);
        final String totalChars = RandomStringUtils.randomAlphanumeric(2);

        final String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(totalChars);

        final List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());

        Collections.shuffle(pwdChars);

        return pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }
}
