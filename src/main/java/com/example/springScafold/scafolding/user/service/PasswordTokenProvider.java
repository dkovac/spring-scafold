package com.example.springScafold.scafolding.user.service;

public interface PasswordTokenProvider {

    String createToken(Long id);

    void validateToken(String token);

    Long getUserId(String token);

    Long getTokenValidity();

}
