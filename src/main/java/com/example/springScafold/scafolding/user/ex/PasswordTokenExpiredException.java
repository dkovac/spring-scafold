package com.example.springScafold.scafolding.user.ex;

public class PasswordTokenExpiredException extends RuntimeException {

    public PasswordTokenExpiredException(String message) {
        super(message);
    }

}
