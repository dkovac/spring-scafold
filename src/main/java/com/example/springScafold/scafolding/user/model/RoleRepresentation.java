package com.example.springScafold.scafolding.user.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class RoleRepresentation {
    private Long id;
    private String code;
    private String description;
}
