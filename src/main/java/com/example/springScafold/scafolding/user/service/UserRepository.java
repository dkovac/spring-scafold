package com.example.springScafold.scafolding.user.service;

import com.example.springScafold.scafolding.common.service.ProjectRepository;
import com.example.springScafold.scafolding.user.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends ProjectRepository<User> {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    @Query("FROM User u " +
            "WHERE (:term = u.username OR :term = u.email) " +
            "AND u.deleted = false")
    @EntityGraph(value = "User.roles", type = EntityGraph.EntityGraphType.LOAD)
    Optional<User> findByUsernameOrEmail(@Param("term") String usernameOrEmail);

}
