package com.example.springScafold.scafolding.user.ex;

public class UserException extends RuntimeException {
    private final String validationMessage;

    public UserException(String message) {
        super(message);
        this.validationMessage = null;
    }

    public UserException(String message, String validationMessage) {
        super(message);
        this.validationMessage = validationMessage;
    }

    public String getValidationMessage() {
        return validationMessage;
    }
}
