package com.example.springScafold.scafolding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ScafoldApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScafoldApplication.class, args);
	}

}
